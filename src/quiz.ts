// on cree une variable question qui va avoir toute les question dans un tableau
let questions = [
    "question1 : Combien de fois les All -Black ont gagné la coupe du monde ?",
    "Question2 : Combien de fois l’Afrique du sud a gagné la coupe du monde? ",
    "Question3 : Combien de fois l’Angleterre a gagné la coupe du monde  ? "
    , "Question4 : Combien de fois la France a fait le Grand Chelem au tournoi des 6 nations ?",
    "Question5 : Jusqu’où la France a été pour la coupe du monde 2023?",
    "Question6 : Qui a gagné la coupe du monde 2023 ?"
    , " Question7 : Qui va rencontrer la France au stade de Lyon pendant le tournoi des 6 nations 2024?",
    "Question8 : Qui va rencontrer la France lors du premier match des 6 nations 2024?",
    "Question9 : qui a gagné le  tournoi des 6 nations en 2022 ?"]

let reponce = [
    ["1", "5", "3", "4"],
    ["1", "4", "6", "2"],
    ["1", "4", "10", "8"],
    ["1", "4", "10", "8"],
    ["Demi-finale", "quart-de finale", "8ème de finale", "phase de poule"],
    ["New Zeland", "Afrique du sud", "France", "Angleterre"],
    ["Irlande", "Italie", "Pays de Galles", "Angletere"],
    ["Irlande", "Angletere", "Pays de Galles", "Italie"],
    ["Irlande", "France", "Pays de Galles", "Angletere"],




]
let restart = document.querySelector<HTMLElement>("#restart")
let fin = document.querySelector<HTMLElement>("#finit")
let quizContainer = document.querySelector<HTMLElement>("#quiz")
let score = document.querySelector<HTMLElement>("#score")
let scoreFinal = 0
let p = document.querySelector<HTMLElement>("#result")
let btnClose = document.querySelector<HTMLElement>(".btn");

let resultQuestion = document.querySelector<HTMLElement>('.modal')


let reponsesCorrect = ["3", "4", "1", "10", "quart-de finale", "Afrique du sud", "Angletere", "Irlande", "France"]

let question1 = document.querySelector<HTMLElement>('h1');

let index = 0;
let RetakeBtn = document.querySelector<HTMLElement>('.Retake-btn');
RetakeBtn.addEventListener("click", (_) => {

    index = -1




    Nextquestion();

})



// lement question 1 on lui assigne comme texte la père valeur du tableau question
question1.textContent = questions[0];
//on crrée une variable button et on lui assigne la classe .option1
let option1 = document.querySelector<HTMLElement>('.option1');
let option = document.querySelectorAll('.option');
for (let btnOption of option) {

    btnOption.addEventListener("click", (_) => {

        resultQuestion.style.display = "block"
        if (btnOption.textContent == (reponsesCorrect[index])) {
            p.textContent = "bonne reponse"
            scoreFinal++
            score.innerHTML = 'Votre score est '+ scoreFinal + "/9"

        }


        else {
            p.textContent = reponsesCorrect [index]
           + ""
           + " mauvaise reponse"
        }

        Nextquestion()
    })
}




option1.textContent = reponce[index][0];


let option2 = document.querySelector<HTMLElement>('.option2');

option2.textContent = reponce[index][1];


let option3 = document
    .querySelector<HTMLElement>('.option3');

option3.textContent = reponce[index][2];

let option4 = document.querySelector<HTMLElement>('.option4');

option4.textContent = reponce[index][3];




//on recuperere dans le html l'element qui a la class .next-btn
let button1 = document.querySelector<HTMLElement>('.next-btn');


button1.addEventListener("click", (_) => {

    Nextquestion()
})


function Nextquestion() {


    if (index < 8) {

        let questionNumber = document.querySelector<HTMLElement>("#question");
        // on incremment la valeur de 1 question
        index++
        // on assigne la valeur index a chaque question
        question1.textContent = questions[index];

        //assigne la première ronponce du tableau index index
        option1.textContent = reponce[index][0];


        option2.textContent = reponce[index][1];



        option3.textContent = reponce[index][2];


        option4.textContent = reponce[index][3];

        questionNumber.textContent =  index + 1 + '/9'
     }
    else {
        quizContainer.style.display = "none";
        fin.style.display = "block"
        score.style.display = "block"

        restart.style.display = "block"
       

    }


   

}



btnClose.addEventListener("click", (_) => {
    resultQuestion.style.display = "none"
});

restart.addEventListener("click", (_) => {
    
    index = -1
resultQuestion.style.display = "none"
 quizContainer.style.display = "block";
 score.style.display = "none";

 fin.style.display = "none"
    scoreFinal =-1
restart.style.display = "none"
    Nextquestion();

});